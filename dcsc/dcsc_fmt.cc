#include <dcsc/dcsc_fmt.hh>
#include <dcsc/dcsc_io.hh>
#include <dcsc/dcs_driver.h>
/**
 * Message Buffer Version 2.0 id.
 * Version 2.0 represented the same functionality as version 1 but
 * with a modified header word format.
 * @ingroup dcsc_msg_buffer_access
 */
#define MSGBUF_VERSION_2      0xa0000000
/**
 * Message Buffer Version 2.2 id.
 * In version 2.2 to flash access commands had been introduced. 
 * @ingroup dcsc_msg_buffer_access
 */
#define MSGBUF_VERSION_2_2    0xb0000000
/** @def FLASH_DCS_ACCESS_REQUIRED_FW_VERSION
 * version 2.2 or higher required 
 */
#define FLASH_DCS_ACCESS_REQUIRED_FW_VERSION 0x00020002 
/** @def COMPRESSED_DATA_REQUIRED_FW_VERSION
 * version 2.2 or higher required 
 */
#define COMPRESSED_DATA_REQUIRED_FW_VERSION 0x00020002 
/** @def MAJOR_VERSION_NO_REG_ADDR
 * minor version number of the firmware 
 */
#define MAJOR_VERSION_NO_REG_ADDR 0x04 
/** @def MINOR_VERSION_NO_REG_ADDR
 * minor version number of the firmware 
 */
#define MINOR_VERSION_NO_REG_ADDR 0x08 

//____________________________________________________________________
const Dcsc::Format::Flags format_v1 = {
  0,		// _valid_from_version           = 0;
  0x0,		// _header_version_bits          = 0x0;
  28,		// _header_version_bitshift      = 28;
  2,		// _frstword_add_numwords        = 2;
  28,		// _frstword_bitshift_cmdid      = 28;
  4,		// _frstword_width_cmdid         = 4;
  27,		// _frstword_bitshift_sftmsg     = 27;
  0,		// _frstword_width_sftmsg        = 0;
  26,		// _frstword_bitshift_datafmt    = 26;
  0,		// _frstword_width_datafmt       = 0;
  0,		// _frstword_bitshift_numwords   = 0;
  16,		// _frstword_width_numwords      = 16;
  16,		// _frstword_bitshift_blknum     = 16;
  8,		// _frstword_width_blknum        = 8;
  24,		// _frstword_bitshift_extension  = 24;
  2,		// _frstword_width_extension     = 2;
  0,		// _frstword_bitshift_mode       = 0;
  0,		// _frstword_width_mode          = 0;
  0xaa55,	// _marker_pattern               = 0xaa55;
  16,		// _marker_width                 = 16;
  16,		// _marker_bitshift              = 16;
  0xdd33,	// _endmarker_pattern            = 0xdd33;
  16,		// _endmarker_width              = 16;
  16,		// _endmarker_bitshift           = 16;
};

//____________________________________________________________________
const Dcsc::Format::Flags format_v2 = {
  0x00020000,      // _valid_from_version           = 0x00020000;
  MSGBUF_VERSION_2,// _header_version_bits          = MSGBUF_VERSION_2;
  0,               // _header_version_bitshift      = 0;
  0,               // _frstword_add_numwords        = 0;
  0,               // _frstword_bitshift_cmdid      = 0;
  6,               // _frstword_width_cmdid         = 6;
  0,               // _frstword_bitshift_sftmsg     = 0;
  0,               // _frstword_width_sftmsg        = 0;
  0,               // _frstword_bitshift_datafmt    = 0;
  0,               // _frstword_width_datafmt       = 0;
  6,               // _frstword_bitshift_numwords   = 6;
  10,              // _frstword_width_numwords      = 10;
  16,              // _frstword_bitshift_blknum     = 16;
  8,               // _frstword_width_blknum        = 8;
  0,               // _frstword_bitshift_extension  = 0;
  0,               // _frstword_width_extension     = 0;
  0,               // _frstword_bitshift_mode       = 0;
  0,               // _frstword_width_mode          = 0;
  0xaa55,          // _marker_pattern               = 0xaa55;
  16,              // _marker_width                 = 16;
  16,              // _marker_bitshift              = 16;
  0xdd33,          // _endmarker_pattern            = 0xdd33;
  16,              // _endmarker_width              = 16;
  16,              // _endmarker_bitshift           = 16;
};

//____________________________________________________________________
const Dcsc::Format::Flags format_v2_2 = {
  0x00020002,         // _valid_from_version           = 0x00020002;
  MSGBUF_VERSION_2_2, // _header_version_bits          = MSGBUF_VERSION_2_2;
  0,                  // _header_version_bitshift      = 0;
  0,                  // _frstword_add_numwords        = 0;
  0,                  // _frstword_bitshift_cmdid      = 0;
  6,                  // _frstword_width_cmdid         = 6;
  0,                  // _frstword_bitshift_sftmsg     = 0;
  0,                  // _frstword_width_sftmsg        = 0;
  24,                 // _frstword_bitshift_datafmt    = 24;
  2,                  // _frstword_width_datafmt       = 2;
  6,                  // _frstword_bitshift_numwords   = 6;
  10,                 // _frstword_width_numwords      = 10;
  16,                 // _frstword_bitshift_blknum     = 16;
  8,                  // _frstword_width_blknum        = 8;
  0,                  // _frstword_bitshift_extension  = 0;
  0,                  // _frstword_width_extension     = 0;
  26,                 // _frstword_bitshift_mode       = 26;
  1,                  // _frstword_width_mode          = 1;
  0xaa55,             // _marker_pattern               = 0xaa55;
  16,                 // _marker_width                 = 16;
  16,                 // _marker_bitshift              = 16;
  0xdd33,             // _endmarker_pattern            = 0xdd33;
  16,                 // _endmarker_width              = 16;
  16,                 // _endmarker_bitshift           = 16;
};
  
//____________________________________________________________________
const Dcsc::Format::Flags* Dcsc::Format::_flags =  0;

//____________________________________________________________________
Dcsc::Format::Format(IO& io) 
{
  int major = 2; // Default choice
  int minor = 0;
  if ((major = io.Control(IOCTL_READ_REG+MAJOR_VERSION_NO_REG_ADDR,0))>=0) {
    if (major > 0) 
      minor = io.Control(IOCTL_READ_REG+MINOR_VERSION_NO_REG_ADDR,0);
  }
  else
    major = 1;
  _fw_version = (major << 16) | minor;

  if (_fw_version >= FLASH_DCS_ACCESS_REQUIRED_FW_VERSION)
    _flash_access = Dcs;
  if (_fw_version >= COMPRESSED_DATA_REQUIRED_FW_VERSION) 
    _compression = true;
  if      (_fw_version >= format_v2_2._valid_from_version) 
    _flags = &format_v2_2;
  else if (_fw_version >= format_v2._valid_from_version) 
    _flags = &format_v2;
  else if (_fw_version >= format_v1._valid_from_version)
    _flags = &format_v1;
  else 
    _flags = &format_v2_2;
}

//____________________________________________________________________
u32_t
Dcsc::Format::Make1st(unsigned int n_words, 
		      unsigned int block_num, 
		      unsigned int format, 
		      unsigned int mode, 
		      unsigned int cmd) const
{
  unsigned short extension = 0x0000;
  u32_t ret = 0;

  ret |= (_flags->_header_version_bits <<_flags->_header_version_bitshift);
  ret |= ((cmd       & ((0x1 << _flags->_frstword_width_cmdid)     - 1))
	  << _flags->_frstword_bitshift_cmdid);
  ret |= ((mode      & ((0x1 << _flags->_frstword_width_mode)      - 1))
	  << _flags->_frstword_bitshift_mode);
  ret |= ((format    & ((0x1 << _flags->_frstword_width_datafmt)   - 1))
	  << _flags->_frstword_bitshift_datafmt);
  ret |= ((extension & ((0x1 << _flags->_frstword_width_extension) - 1))
	  << _flags->_frstword_bitshift_extension);
  ret |= ((block_num & ((0x1 << _flags->_frstword_width_blknum)    - 1))
	  << _flags->_frstword_bitshift_blknum);
  ret |= (((n_words  + _flags->_frstword_add_numwords) 
	   & ((0x1 << _flags->_frstword_width_numwords)-1))
	  << _flags->_frstword_bitshift_numwords);
  return  ret;
}

//____________________________________________________________________
u32_t
Dcsc::Format::MakeLast(unsigned short check_sum) const
{
  return (_flags->_marker_pattern << _flags->_marker_bitshift) | check_sum;
}
  
//____________________________________________________________________
u32_t
Dcsc::Format::MakeMarker(unsigned short unused) const
{
  unused = 0;
  return (_flags->_endmarker_pattern << _flags->_endmarker_bitshift) | unused;
}

//____________________________________________________________________
//
// EOF
//
