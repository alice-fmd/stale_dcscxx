#ifndef DCSC_DEV_HH
#define DCSC_DEV_HH
#include <vector>
#include <dcsc/dcsc_buf.hh>
#include <dcsc/dcsc_io.hh>
#include <dcsc/dcsc_fmt.hh>

namespace Dcsc 
{
  class Driver;

  class Device 
  {
    enum {
      /** 
       * Initialization flag: force message buffer format version v1
       * This sets the encoding to version 1 fixed rather than trying
       * to get the correct firmware version.
       */
      ForceV1 = 0x0001, 
      /** 
       * Initialization flag: force message buffer format version v2.
       * This sets the encoding to version 2 fixed rather than trying
       * to get the correct firmware version.
       */
      ForceV2 = 0x0002,
      /** 
       * Initialization flag: write block to device/file but do not
       * execute.  The interface incodes the message buffer and writes
       * it to the device/ file but does not execute the command.
       */
      Encode = 0x0100, 
      /** 
       * Initialization flag:  Append to file.
       * When in 'encoding' mode, the data is appended to the file. 
       */
      Append = 0x0200, 
      /** 
       * Initialization flag: suppress stderr output.
       * The stderr output is completely suppressed.
       */
      SuppressErr = 0x0400,
      /** 
       * Initialization flag: skip automatic adaption to driver
       * properties.  By default the interfaces ties to fetch the
       * charactaristics of the driver during initialization (MIB size,
       * version , ...). This flag causes the interface to skip the
       * adaption.  
       */
      SkipAdaption = 0x1000
    };
      
    /** 
     * Extended initialization.
     * The function allows beside the device name a couple of other
     * parameters to adjust the interface behavior.
     * @param dev  name of the device node, if NULL, use @c
     *             /dev/dcsc. 
     * @param  flags logical or of the init flags flags
     * @param  size  size of the MIB, used for encoding of message blocks 
     * @return  neg. error code if failed<br>
     *   -ENOSPC can not get the size of the interface buffers, or 
     *           buffers too small
     *   -ENOENT can not open device
     */
    Device(const std::string& dev="/dev/rcu/msgbuf", 
	   int flags=0, int size=-1, int verb=0);

    /** Reset the simulation. Reset all the internal variables of the
     * register simulation and seek to beginning of the register
     * files.*/
    void Reset() {}

    /** 
     * Close the device and release internal data structures.
     * @return neg. error code if failed<br>
     *   -ENXIO : no device open
     */
    virtual ~Device();

    /** Write a single location (32bit word)
     * @param address   16 bit address in RCU memory space
     * @param data      data word
     * @return
     */
    int Write(u32_t addr, u32_t data){return 0;}
    /**
     * Read a single location (32bit word)
     * @param address   16 bit address in RCU memory space
     * @param data      buffer to receive the data
     * @return
     * @ingroup dcsc_msg_buffer_access
     */
    int Read(u32_t addr, u32_t& data){return 0;}

    /** Word sizes */
    enum WordSize {
      /** 8 bits */
      Word8  = 1, 
      /** 16 bits */
      Word16 = 2, 
      /** 10 bits (compressed) */
      Word10 = 3,
      /** 32 bits */
      Word32 = 4, 
      /** Swapped 16 bits */
      Swapped16 = 5, 
      /** Swapped 32 bits */
      Swapped32 = 6 
    };
    
    /** Write a number of 32bit words beginning at a location. 
     * The function takes care for the size of the MIB and splits the 
     * operation if the amount of data to write exceeds the MIB size.  
     * The function expects data in little endian byte order
     * @param address   16 bit address in RCU memory space
     * @param data      buffer containing the data
     * @param wsize     Size of one word in bytes, given by enum.
     * @return
     */
    int Write(u32_t address, const std::vector<u32_t>& data, WordSize wsize){return 0;}

    /**
     * Read a number of 32bit words beginning at a location.
     * @param address   16 bit address in RCU memory space
     * @param size      number of words to write
     * @param data      buffer to receive the data, the function will
     *                  adjust it to a suitable size (i.e. size x
     *                  wordsize) 
     * @return          number of 32bit words which have been read,
     *                  neg. error code if failed 
     */
    int Read(u32_t address, int size, std::vector<u32_t>& data){return 0;}

    /**
     * Provide the message buffer for direct access.
     * Creation and destruction of the buffer is handled by the interface
     * internally <b>DO NOT DELETE THIS BUFFER</b>.<br> 
     * <b>Note:</b> This function is forseen but not yet implemented  
     * @param  buffer    target to receive the buffer pointer
     * @param  size      target to receive the buffer size
     * @return pointer to buffer, the caller is supposed to encode the 
     *         complete message buffer, including the information word,
     *         the markers and data as well as check sum. The interface
     *         does not alter it. 
     */
    int GetMessageBuffer(u32_t*& buffer, u32_t& size){return 0;}

    /**
     * Prepare the message buffer for a specific command.
     * Creation and destruction of the buffer is handled by the
     * interface internally 
     * <b>DO NOT DELETE THIS BUFFER</b>.<br>
     * <b>Note:</b> This function is forseen but not yet implemented
     * @param  buffer    target to receive the buffer pointer
     * @param  size      target to receive the buffer size
     * @param  cmd       command ID to prepare the buffer for
     * @param  flags     for future extensions (e.g. preparation for 
     *                   compressed pedestal data) 
     * @return pointer to buffer where the data words can directly be 
     *         stored to, the information word and the markers and
     *         check sum are written by the interface. */
    int PrepareMessageBuffer(u32_t*& buffer, u32_t& size, 
			     unsigned int cmd, unsigned int flags){return 0;}

    /**
     * Execute the message buffer command. 
     * Pointer and size are required in order to cross check the
     * buffer arguments.
     * 
     * This tool is going to be used also for the encoding of
     * configuration data, it might be usefull to write the ready
     * prepared command buffer to a file or pipe. In that sense no
     * execution and result interpretation is needed, this will be
     * controlled by the operation flags.
     *
     * @note function is forseen but not yet implemented
     *
     * @param  command    pointer to command buffer to execute
     * @param  size       size of the command buffer
     * @param  target     target to receive the result buffer
     *                    (e.g. for read operations) 
     * @param  osize      target to receive the size of the result buffer 
     * @param  operation  flags for operation control (for future
     *                    extension)  
     * @return result buffer if available 
     */
    int Execute(u32_t* buffer, int size, u32_t*& target, int& osize, 
		unsigned int operation){return 0;}

    /**
     * Operation specifier for the HeaderAttribute function.  
     */
    enum {
      UnknownSpec = 0,
      Version,
      Command,
      NofWords,
      Packed,
    };

    /**
     * Extract a part of the header word according to the specifier. 
     * @param header     the header word
     * @param specifier  specifier for the part to extract
     * @return extracted part, neg. error code if failed
     */
    int HeaderAttribute(u32_t header, int iSpecifier){return 0;}

    /**
     * Check a message block for correct format.
     * @param  buffer    pointer to message block
     * @param  size      size of the block in 32bit words
     * @param  verbosity 0: no messages, 1: warnings for format
     *                   missmatch, 2: debug messages 
     * @return >0 check successful, return block length in 32bit
     *            words<br> 
     *         =0 unknown format
     *         <0 other errors
     */
    int CheckMessageBlock(u32_t buffer, int size, int iVerbosity){return 0;}
  protected:
    /** verbosity */
    int _verbose;
    /** Flags */
    int _flags;
    
    /** I/O interface */
    IO _io;

    /** @{
	@name Buffers */
    /** The memory */
    u32_t* _buffer;
    /** Input buffer */
    InputBuffer _in_buffer;
    /** output buffer */
    OutputBuffer _out_buffer;
    /** Registe buffer */
    RegisterBuffer _reg_buffer;
    /** @} */

    /** Format */ 
    Format _format;
  };
}

#endif
//
// EOF
//



    
	 
    
    
