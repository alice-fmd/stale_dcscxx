#ifndef DCSC_FMT_HH
#define DCSC_FMT_HH

namespace Dcsc
{
  // Forward declaration 
  class IO;

  /** @class Format dcsc_fmt.hh <dcsc/dcsc_fmt.hh>
      @brief Formatting of buffers 
      @ingroup buffers 
  */
  class Format 
  {
  public:
    /** Constructor 
	@param io Reference to low-level I/O interface */
    Format(IO& io);

    /** Make 1st word of a command structure 
	@param n_words Number of words 
	@param block_num Number of blocks 
	@param format Format code 
	@param cmd Command */
    u32_t Make1st(unsigned int n_words, 
		  unsigned int block_num, 
		  unsigned int format, 
		  unsigned int mode, 
		  unsigned int cmd) const;
    /** Make last marker */
    u32_t MakeLast(unsigned short check_sum) const;
    /** Make end marker */
    u32_t MakeMarker(unsigned short unused) const;

    /** Access mode for flash */
    enum FlashMode {
      /** access via flash interface of the RCU Actel */
      Actel, 
      /** access via the DCS board firmware */
      Dcs
    } _flash_access;

    /** Enable compression for msg buffer transactions */
    bool _compression;
    /** Firmware version */
    int _fw_version;

    /** @class Flags dcsc_fmt.hh <dcsc/dcsc_fmt.hh>
	@brief flags for formatting 
	@ingroup buffers 
    */
    struct Flags 
    {
      /** first version this header format becomes valid */
      int _valid_from_version;
      /**  */
      int _header_version_bits;
      /**  */
      int _header_version_bitshift;
      /** number of additional words beside the regular words of a
	  certain command */ 
      int _frstword_add_numwords;
      /** position of the command id */
      int _frstword_bitshift_cmdid;
      /** width of the command id */
      int _frstword_width_cmdid;
      /** position of the safety message (defined in the v1 format, most
	  likely never used) */ 
      int _frstword_bitshift_sftmsg;
      /** width of the safety message */
      int _frstword_width_sftmsg;
      /** position of the data format */
      int _frstword_bitshift_datafmt;
      /** width of the data format */
      int _frstword_width_datafmt;
      /** position of the data word count field  */
      int _frstword_bitshift_numwords;
      /** width of the data word count field */
      int _frstword_width_numwords;
      /** position of the block counter field */
      int _frstword_bitshift_blknum;
      /** width of the block counter field */
      int _frstword_width_blknum;
      /** defined in the v1 msg bufer specification, most likely never
	  used */ 
      int _frstword_bitshift_extension;
      /** dito */
      int _frstword_width_extension;
      /** position of the mode field: rcu memory, flash, select map */
      int _frstword_bitshift_mode;
      /** width of the mode field */
      int _frstword_width_mode;
      /**  */
      int _marker_pattern;
      /**  */
      int _marker_width;
      /**  */
      int _marker_bitshift;
      /**  */
      int _endmarker_pattern;
      /**  */
      int _endmarker_width;
      /**  */
      int _endmarker_bitshift;
    };
  protected:
    /** The header format in use */
    static const Flags* _flags;
  };
}

#endif
//
// EOF
//



