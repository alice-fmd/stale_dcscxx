#include <dcsc/dcsc_dev.hh>
#include <stdexcept>
#include <cerrno>
#include <sstream>

//____________________________________________________________________
struct sys_exception : std::exception
{
  sys_exception(const std::string msg) 
  {
    std::stringstream s;
    if (!msg.empty())
      s << msg << ": ";
    s << strerror(errno);
    _what = s.str();
  }
  virtual ~sys_exception() throw () {}
  const char* what() const throw () { return _what.c_str(); }
protected:
  std::string _what;
};

//____________________________________________________________________
Dcsc::Device::Device(const std::string& name, int flags, 
		     int size, int verbose) 
  : _flags((size > 0 ? SkipAdaption : 0) | flags), 
    _verbose(verbose),
    _io(name, flags & Append), 
    _buffer(0),
    _in_buffer(_io,  _verbose, size), 
    _out_buffer(_io, _verbose, flags & SkipAdaption), 
    _reg_buffer(_io, _verbose, flags & SkipAdaption), 
    _format(_io)
{
  if (!_io.IsOpen()) return;

  if (_in_buffer._size < 24  || 
      _out_buffer._size < 12 || 
      _reg_buffer._size < 1) return;
      
  if (_flags & Encode && _verbose) 
    std::cerr << "Set DCSC_INIT_ENCODE flag" << std::endl;
  if (_flags & Append && _verbose) 
    std::cerr << "Set DCSC_INIT_APPEND flag" << std::endl;

  size_t total = _in_buffer._size + _out_buffer._size + _reg_buffer._size;
  _buffer = new u32_t[total];
  _in_buffer.Assign(_buffer, 0);
  _out_buffer.Assign(_buffer, _in_buffer._size);
  _reg_buffer.Assign(_buffer, _out_buffer._offset + _out_buffer._size);

  // Here, we should do something like 
#if 0
  // work around for DCSC_TEST case: the register can not be read if
  // a new file is used instead of the driver, this just writes
  // something to the last register position and thus creates the
  // file of appropriate length
  if (readDcscRegister(GENERAL_CTRL_REG_ADDR, 0)<0 && 
      (g_dcscFlags&DCSC_INIT_ENCODE)==0) {
#ifdef DCSC_TEST
    writeDcscRegister(REGISTER_3_ADDR, 0);
#else  //DCSC_TEST
    fprintf(stderr, "hardware access error\n");
    closeDevice();
    iResult=-EIO;
#endif //DCSC_TEST
  }
  printDriverInfo(0); // print a short driver info
#endif
}

//____________________________________________________________________
Dcsc::Device::~Device()
{
  if (_buffer) delete [] _buffer;
}

  
//____________________________________________________________________
//
// EOF
//
