#include <dcsc/dcsc_buf.hh>
#include <dcsc/dcsc_io.hh>
#include "dcs_driver.h"
#include <iostream>
#include <iomanip>

//====================================================================
Dcsc::Buffer::Buffer(IO& io, bool verb, bool skip, int request, size_t defsize)
  : _io(io), _size(defsize), _offset(0), _ptr(0)
{
  if (skip) return;
  int val;
  if (_io.Control(request, &val) >= 0) _size = val;
  else if (verb)
    std::cerr << "Failed to get buffer size, using default value of 0x" 
	      << std::hex << _size << std::endl;
}

//====================================================================
Dcsc::InputBuffer::InputBuffer(IO& io, bool verb, size_t fixed) 
  : Buffer(io, verb, fixed > 0, 
	   IOCTL_GET_MSGBUF_IN_SIZE, (fixed > 0 ? fixed : 0x40))
{}

//====================================================================
Dcsc::OutputBuffer::OutputBuffer(IO& io, bool verb, bool skip) 
  : Buffer(io, verb, skip, IOCTL_GET_MSGBUF_OUT_SIZE, 0x40)
{}

//====================================================================
Dcsc::RegisterBuffer::RegisterBuffer(IO& io, bool verb, bool skip) 
  : Buffer(io, verb, skip, IOCTL_GET_REGFILE_SIZE, 0x10)
{}


//====================================================================
//
// EOF
//

  
