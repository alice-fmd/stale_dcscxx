#ifndef DCSC_IO_HH
#define DCSC_IO_HH
#include <string>
#include <vector>

// Forward decl
extern "C" 
{
  extern int ioctl(int,long unsigned int,...) throw ();
}

namespace Dcsc 
{
  /** @class IO dcsc_io.hh <dcsc/dcsc_io.hh>
      @brief Defines low-level I/O access to the device file 
      @ingroup low 
  */
  class IO 
  {
  public:
    /** Constructor 
	@param file file or device to open
	@param append Open in append mode.
	@exception std::runtime_error Thrown in case of problems */
    IO(const std::string& file, bool append=false);
    /** Destructor */ 
    virtual ~IO();
    
    enum { 
      /** Returned on success */
      Success,
      /** Operation not supported */
      NotSupported, 
      /** I/O error */
      Error, 
      /** Bad file */
      BadFile,
      /** Location is out of bounds */
      OutOfBounds,
      /** Invalid parameter */
      InvalidParam, 
      /** Stream is not a TTY */
      NotTTY, 
      /** Try again */ 
      Again, 
      /** Not locked */ 
      NoLock
    };

    /** Control device (ioctl). 
	@param request The request. 
	@param arg Possible argument to request 
	@return 0 on success, negative error otherwise */ 
    template <typename T>
    int Control(int request, T arg);

    /** Test if file is opened 
	@return true if the file is opened - false otherwise */
    bool IsOpen() const { return _file > 0; }

    /** Seek to a particular position in the file.  Note, that the
	driver does not support seeking to other locations than the
	start - hence the second argument is always ignored. 
	@param off Offset (from start) to seek to 
	@param start Offset (from start of file) to seek from. 
	@return 0 on succes, otherwise negative error code. 
    */
    int Seek(long offset, int start=0);

    /** Write data to the device at the current position (as set by
	Seek). 
	@param ptr Start location pointer. 
	@param word_size Size of words (in bytes). 
	@param n Number of words to write to the file. 
	@return number of words written to device, or negative error
	code. */
    int Write(const void* ptr, size_t word_size, size_t n);

    /** Write to the device.  As the member function that takes a @c
	void* object, except that the word size is deduced from the
	data type 
	@param ptr Pointer to data to write
	@param n Number of words to write 
	@return number of words written to device, or negative error
	code. */ 
    template <typename T>
    int Write(const T* ptr, size_t n);
    
    /** Write to the device.  As the member function that takes a @c
	void* object, except that the word size is deduced from the
	data type, and the number of words to write is the size of the
	vector passed. 
	@param data Data to write
	@param n If @a n is not negative, and less than the size of @a
	data, only @a n words will be written.  Otherwise, the full
	content of @a data will be written. 
	@return number of words written to device, or negative error
	code. */ 
    template <typename T>
    int Write(const std::vector<T>& data, size_t n=0);

    /** Read data at current position (as set by Seek) from the
	device. 
	@param ptr Location to read into. 
	@param word_size Size of words (in bytes)
	@param n Number of words to read. 
	@return Number of words read, or negative error code */
    int Read(void* ptr, size_t word_size, size_t n);

    /** Read data from the device.  As the member function that takes
	a @c void* as first argument, except that the word size is
	deduced from the data type. 
	@param ptr Address to read data into. 
	@param n Number of words to read. 
	@return Number of words read, or negative error code */
    template <typename T>
    int Read(T* ptr, size_t n);

    /** Read data from the device.  As the member function that takes
	a @c void* as first argument, except that the word size is
	deduced from the data type.  Further more, the number of words
	read is either the same as the vector size (if @a n is not
	positive) or @a n words, in which case @a data is resized
	appropriately 
	@param data Vector to read data into. 
	@param n If @a n <= 0, this is ignored.  Otherwise the number
	of words to read, and @a data is resized to be able to contain
	at least that many words. 
	@return Number of words read, or negative error code */
    template <typename T>
    int Read(std::vector<T>& data, size_t n=0);

    /** Lock access to the device to this process. 
	@return 0 on success, negative error code otherwise */
    int Lock();

    /** Un-lock access to the device to this process. 
	@return 0 on success, negative error code otherwise */
    int Unlock();

    /** Seize the device 
	@return 0 on success, negative error code otherwise */
    int Seize();

    /** Release the device 
	@return 0 on success, negative error code otherwise */
    int Release();
    
    /** Reset lock on device 
	@return 0 on success, negative error code otherwise */
    int ResetLock();

    /** Activate lock on device 
	@return 0 on success, negative error code otherwise */
    int ActivateLock();

    /** Set debugging level 
	@param flags Debug flags 
	@return 0 on success, negative error code otherwise */
    int Debug(unsigned int flags);

  protected:
    /** Handle a return value, and translate to our errors */ 
    int HandleReturn(int ret);
    /** The file number */
    int _file;
    /** The current application number - used in locks */
    static int _app_id;
  };

  //__________________________________________________________________
  template <typename T> 
  inline int 
  IO::Control(int request, T arg) 
  {
    if (!IsOpen()) return -BadFile;
    return HandleReturn(ioctl(_file, request, arg));
  }

  //__________________________________________________________________
  template <typename T> 
  inline int 
  IO::Write(const T* ptr, size_t n) 
  {
    return Write((void*)ptr, sizeof(T), n);
  }
  //__________________________________________________________________
  template <typename T> 
  inline int 
  IO::Write(const std::vector<T>& data, size_t n) 
  {
    if (n <= 0 || n >= data.size()) n = data.size();
    return Write((void*)(&(data[0])), sizeof(T), n);
  }
  //__________________________________________________________________
  template <typename T> 
  inline int 
  IO::Read(T* ptr, size_t n) 
  {
    return Read((void*)ptr, sizeof(T), n);
  }
  //__________________________________________________________________
  template <typename T> 
  inline int 
  IO::Read(std::vector<T>& data, size_t n) 
  {
    if (n > 0 && data.size() < n) data.resize(n);
    else n = data.size();
    return Read((void*)(&(data[0])), sizeof(T), n);
  }

}

#endif
//
// EOF
//

