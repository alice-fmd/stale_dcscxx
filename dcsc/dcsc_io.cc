#include <dcsc/dcsc_io.hh>
#include <cerrno>
#include "dcs_driver.h"
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>

//____________________________________________________________________
int Dcsc::IO::_app_id = 0;

//____________________________________________________________________
Dcsc::IO::IO(const std::string& name, bool append)
  : _file(-1)
{
  int flags = O_RDWR;
  int mode  = 0;

  if (name != "/dev/") {
    flags |= O_CREAT;
    if (append) flags |= O_APPEND;
    mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;
  }
  _file = open(name.c_str(), flags, mode);
  if (_file <= 0) return;
}

//____________________________________________________________________
Dcsc::IO::~IO()
{
  if (IsOpen()) close(_file);
  _file = 0;
}

//____________________________________________________________________
int
Dcsc::IO::Seek(long off, int start)
{
  if (start)     return -NotSupported;
  if (!IsOpen()) return -BadFile;
  
  off_t result = lseek(_file, off, SEEK_SET);
  if (result != off) return -Error;
  return 0;
}

//____________________________________________________________________
int
Dcsc::IO::Write(const void* ptr, size_t word_size, size_t n)
{
  if (!IsOpen()) return -BadFile;
  
  ssize_t result = write(_file, ptr, word_size * n);
  if (result < 0) return -Error;
  return result / word_size;
}

//____________________________________________________________________
int
Dcsc::IO::Read(void* ptr, size_t word_size, size_t n)
{
  if (!IsOpen()) return -BadFile;
  
  ssize_t result = read(_file, ptr, word_size * n);
  return result / word_size;
}

//____________________________________________________________________
int
Dcsc::IO::Lock()
{
  return Control(IOCTL_LOCK_DRIVER, _app_id);
}

//____________________________________________________________________
int
Dcsc::IO::Unlock()
{
  return Control(IOCTL_UNLOCK_DRIVER, _app_id);
}

//____________________________________________________________________
int
Dcsc::IO::Seize()
{
  if (!IsOpen()) return -BadFile;
  if (_app_id != 0) return -Again;
  
  int lock_code = time(NULL);
  if (lock_code <= 0) return -NoLock;

  int ret = Control(IOCTL_SEIZE_DRIVER, _app_id);
  _app_id = lock_code;
  return ret;
}

//____________________________________________________________________
int
Dcsc::IO::Release()
{
  if (!IsOpen()) return -BadFile;
  if (_app_id <= 0) return -NoLock;
  
  int ret = Control(IOCTL_RELEASE_DRIVER, _app_id);
  _app_id = 0;
  return ret;
}

//____________________________________________________________________
int
Dcsc::IO::ResetLock()
{
  return Control(IOCTL_RESET_LOCK, 0);
}

//____________________________________________________________________
int
Dcsc::IO::ActivateLock()
{
  return Control(IOCTL_ACTIVATE_LOCK, 0);
}

//____________________________________________________________________
int
Dcsc::IO::Debug(unsigned int flags)
{
  return Control(IOCTL_SET_DEBUG_LEVEL, flags);
}

//____________________________________________________________________
int
Dcsc::IO::HandleReturn(int ret)
{
  if (ret < 0) {
    if (ret == -1) ret = errno;
    switch (-ret) {
    case 0:		return Success;		// Returned on success
    case ENOSYS:	return NotSupported;	// Operation not supported
    case EIO:		return Error;		// I/O error
    case EBADF:		return BadFile;		// Bad file
    case EFAULT:	return OutOfBounds;	// Location is out of bounds
    case EINVAL:	return InvalidParam;	// Invalid parameter
    case ENOTTY:	return NotTTY;		// Stream is not a TTY
    case EAGAIN:	return Again;		// Try again
    case ENOLCK:	return NoLock;		// Not locked
    }
  }
  return ret;
}

//____________________________________________________________________
//
// EOF
//
