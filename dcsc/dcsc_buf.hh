#ifndef DCSC_BUF_HH
#define DCSC_BUF_HH
#include <cctype>

namespace Dcsc
{
  // Forward decl 
  class IO;

  /** @defgroup buffers Buffers 
      @brief Interface classes for input, output, and register buffers
  */ 
  //__________________________________________________________________
  /** @class Buffer dcsc_buf.hh <dcsc/dcsc_buf.hh>
      @brief Base class for buffers 
      @ingroup buffers 
  */
  struct Buffer 
  {
    /** Size of buffer (in 32 bit words) */
    size_t _size;
    /** Offset from the beginning (in 32bit words) */
    size_t _offset;
    /** Pointer memory */
    u32_t* _ptr;
    /** Destructor */
    virtual ~Buffer() {}
    /** Set offset and ptr */ 
    void Assign(u32_t* buf, size_t off) 
    {
      _offset = off;
      _ptr    = &(buf[off]);
    }
  protected:
    /** Reference to IO interface */
    IO& _io;
    /** Protected constructor */ 
    Buffer(IO& io, bool verb, bool skip, int request, size_t defsize=0);
  };

  //__________________________________________________________________
  /** @struct InputBuffer dcsc_buf.hh <dcsc/dcsc_buf.hh>
      @brief Input buffers 
      @ingroup buffers 
  */
  struct InputBuffer : public Buffer 
  {
    /** Constructor */
    InputBuffer(IO& io, bool verb, size_t fixed);
    /** Destructor */
    virtual ~InputBuffer() {}
  };

  //__________________________________________________________________
  /** @struct OutputBuffer dcsc_buf.hh <dcsc/dcsc_buf.hh>
      @brief Output buffers 
      @ingroup buffers 
  */
  struct OutputBuffer : public Buffer 
  {
    /** Constructor */
    OutputBuffer(IO& io, bool verb, bool skip);
    /** Destructor */
    virtual ~OutputBuffer() {}
  };

  //__________________________________________________________________
  /** @struct RegisterBuffer dcsc_buf.hh <dcsc/dcsc_buf.hh>
      @brief Register buffers 
      @ingroup buffers 
  */
  struct RegisterBuffer : public Buffer 
  {
    /** Constructor */
    RegisterBuffer(IO& io, bool verb, bool skip);
    /** Destructor */
    virtual ~RegisterBuffer() {}
  };
}
#endif
//
// EOF
//
